﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Excercise_4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            submitButton.Click += new EventHandler(submitConversion);  //Checking for submit click
        }

        private void submitConversion(object sender, EventArgs e)
        {
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            String rawInput = input.Text;  //Grabbing input
            int convertedInput;  //Creating an int to store the converted inputs
            Boolean testParse = int.TryParse(rawInput, out convertedInput);  //Creating a boolean that test's whether the parse will be sucessful
            int rawOutput; //Creating an int to store raw output
            string convertedOutput; //Creating a string to store converted output



            if (rawInput == "" || rawInput == null)  //Checking for no input
            {
                string errorTitle = "No Input";  //Title for alert window
                string errorMessage = "Enter in an amount of seconds";  //Message for alert window
                MessageBoxButtons button = MessageBoxButtons.OK; //Button for alert window
                DialogResult result;  //Setting up alert window
                result = MessageBox.Show(errorMessage, errorTitle, button);  //Running alert window
            }
            else  //If input is not empty
            {
                if (testParse)  //Testing if parse is a success
                {
                    if (convertedInput >= 86400)  //If time is more than or equal to a day
                    {
                        rawOutput = convertedInput / 86400; //Dividing
                        convertedOutput = "" + rawOutput.ToString() + " D";  //Storing
                    }
                    else if (convertedInput >= 3600)  //If time is more than or equal to a hour
                    {
                        rawOutput = convertedInput / 3600; //Dividing
                        convertedOutput = "" + rawOutput.ToString() + " Hr";  //Storing
                    }
                    else if (convertedInput >= 60)  //If time is more than or equal to a minute
                    {
                        rawOutput = convertedInput / 60; //Dividing
                        convertedOutput = "" + rawOutput.ToString() + " Min";  //Storing
                    }
                    else  //If time is less than a minute
                    { 
                        rawOutput = convertedInput; //Dividing
                        convertedOutput = "" + rawOutput.ToString() + " Sec";  //Storing
                    }
                    output.Text = convertedOutput;  //Outputting text
                }
                else
                {
                    string errorTitle = "Could Not Convert Input";  //Title for alert window
                    string errorMessage = "Please make sure to input only numeric values";  //Message for alert window
                    MessageBoxButtons button = MessageBoxButtons.OK; //Button for alert window
                    DialogResult result;  //Setting up alert window
                    result = MessageBox.Show(errorMessage, errorTitle, button);  //Running alert window
                }
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
